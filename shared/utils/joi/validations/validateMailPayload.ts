import { IMailPayload } from "shared/types/mailer";
import { MailPayloadSchema } from "../schemas";

export default function validatePayload(payload: IMailPayload): void {
    const { error } = MailPayloadSchema.validate(payload);
    if (error)
        throw new Error(
            `Validation error: ${error?.details?.map((e) => e.message).join(",\n")}`
        );
}