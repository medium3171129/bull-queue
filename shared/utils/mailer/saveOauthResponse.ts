
import dotenv from "dotenv"
import path from "path"
import { authenticate } from "@google-cloud/local-auth";
import fs from "fs";

dotenv.config()

export default async function saveOauthResponse() {
  try {
    // Obtain user credentials to use for the request
    const auth = await authenticate({
      keyfilePath: path.resolve("./", process.env.OAUTH_KEY_PATH ?? "./.gcp/oauthMailKey.json"),
      scopes: ["https://www.googleapis.com/auth/gmail.send"],
    });

    if (auth.credentials)
      fs.promises.writeFile(
        process.env.OAUTH_TOKEN_PATH ?? "./.gcp/oauthMailToken.json",
        JSON.stringify(auth, null, 4)
      );
  } catch (err) {
    console.log(err);
  }
};
