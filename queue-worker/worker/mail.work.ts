import IORedis from 'ioredis';
import { Worker } from 'bullmq';
import { QueueTypes } from 'shared/enums';
import Gmail from 'shared/services/gmail.service';
import Redis from 'shared/services/redis.service';

const GmailInstance: Gmail = Gmail.bootstrap();
const RedisInstance: IORedis = Redis.bootstrap();

export default function startMailWorker(channel) {
    const mailWorker = new Worker(QueueTypes.MAIL, async job => {
        console.log("Perform Sending email")
        console.log("Job content")
        console.log(job)

        let gmailResponse: any = await GmailInstance.sendMail({
            ...job.data,
            sender: {
                name: process.env.MAIL_SENDER_NAME,
                email: process.env.MAIL_SENDER_EMAIL
            },
        });

        console.log("Send email successfully");
        console.log(gmailResponse?.data);

    }, {
        prefix: channel,
        connection: RedisInstance
    });

    mailWorker.on('ready', () => {
        console.log(`[${channel}] Mail worker ready and listen on queue: ${QueueTypes.MAIL}`)
    });


    mailWorker.on('completed', (job) => {
        console.log(`sending email success: ${JSON.stringify(job)}`);
    });

    mailWorker.on('failed', (job, err) => {
        console.error(`${job?.id} has failed with ${err.message}`);
    });
}