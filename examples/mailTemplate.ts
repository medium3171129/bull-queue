import { IMailPayload } from "shared/types/mailer";

const template: IMailPayload = {
  sender: {
    name: "CW",
    // TODO: USE THE AUTHORIZED EMAIL WHO GRANT PERMISSION TO SEND EMAIL
    email: "faceinattendanceapp@gmail.com"
  },
  receipents: [{ name: "CW", email: "faceinattendanceapp@gmail.com" }],
  subject: "Demo Mail",
  html: `
        <style>
        h1 {
          color: blue;
        }
        </style>
        <h1>header 1</h1>
        <h2>header 2</h2>
        <h3>header 3</h3>
        <h4>header 4</h4>
        <h5>header 5</h5>
        <b>Bold</b>
        <i>Italic</i>
        <hr />
        <p style=3D "color: red; text-decoration: underline">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
    
        <p>Link: <a href=3D "https://google.com">Google</a></p>
    
        <p>Regards,</p>
        Google Inc.
        <br />
        <img src=3D "https://pngimg.com/uploads/google/google_PNG19625.png" width=3D "300" height=3D "100" />
        `,
  attachments: [
    "./data/invoice.pdf",
    "./data/invoice3.pdf",
    "./data/photo.jpeg",
  ],
};

export default template;
