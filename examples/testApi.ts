import axios from "axios"
import template from "./mailTemplate"

async function sendMail() {
    await axios.post(`http://localhost:8080/api/mail`,
        { ...template, subject: "[app1] " + template.subject },
        {
            headers: {
                "x-channel-id": "app1"
            }
        })

    await axios.post(`http://localhost:8080/api/mail`,
        { ...template, subject: "[app2] " + template.subject },
        {
            headers: {
                "x-channel-id": "app2"
            }
        })
}

console.log("Sending mail")
sendMail().then(() => {
    console.log("Success sent mail")
}).catch(e => console.log(e?.response?.data))