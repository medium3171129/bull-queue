module.exports = {
    singleQuote: true,
    trailingComma: 'none',
    semi: false,
    endOfLine: 'lf',
    printWidth: 80
  }
  