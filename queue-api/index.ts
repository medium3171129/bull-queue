import express, { Application } from "express";
import path from "path";
import cors, { CorsOptions } from 'cors';
import router from "./router";

export default class QueueApi {
    app: Application;

    constructor() {
        this.app = express();

        const corsOptions: CorsOptions = {
            origin: function (origin, callback) {
                callback(null, true);
            },
        };


        this.app.use(cors(corsOptions));

        // Configure Express to use EJS
        this.app.set("views", path.join(__dirname, "views"));
        this.app.set("view engine", "ejs");
        this.app.use(express.json({ limit: "1gb" }));
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(express.static(path.join("./", 'public')));

        // define a route handler for the default home page
        this.app.get("/", (req, res) => {
            // render the index template
            res.render("index");
        });

        this.app.use("/api", router);

        /* Error handler middleware */
        this.app.use((req, res, next) => {
            const statusCode = 404;
            return res.status(statusCode).json({ code: statusCode, message: "API Not Found" });
        });

        this.app.use((err, req, res, next) => {
            const statusCode = err.statusCode || 500;
            return res.status(statusCode).json({ code: statusCode, message: err.message });
        });
    }

    listen(port: number) {
        // start the express server
        this.app.listen(port, () => {
            // tslint:disable-next-line:no-console
            console.log(`server started at http://localhost:${port}`);

        });

        this.app._router.stack.forEach(function (r) {
            if (r.route && r.route.path) {
                console.log(r.route.path)
            }
        });
    }
}