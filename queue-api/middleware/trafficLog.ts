import { NextFunction, Request, Response } from 'express';

export default (request: Request, response: Response, next: NextFunction) => {
    console.log(`Client Address: ${request.ip}, ${request.method} ${request.path}`)
    next()
}