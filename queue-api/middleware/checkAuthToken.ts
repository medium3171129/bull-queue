import { NextFunction, Request, Response } from 'express';

export default (request: Request, response: Response, next: NextFunction) => {
    // const authHeader = request.headers['x-api-key'];
    // const token = authHeader && authHeader.split(' ')[1];

    // if (token == null) {
    //     return request.status(200).json({ err: 'Request new Token' });
    // }

    // let user = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    // req.user = user;
    next();
}