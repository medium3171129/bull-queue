import { Router } from "express";
import { checkAuthToken, trafficLog } from "../middleware";
import mailRouter from "./mail"
import jobRouter from "./job"

const router: Router = Router();

router.use("/mail", trafficLog, checkAuthToken, mailRouter)
router.use("/job", trafficLog, checkAuthToken, jobRouter)


export default router