import type { NextFunction, Request, Response } from "express";
import { Router } from "express";
import * as utils from 'shared/utils';
import QueueService from "shared/services/queue.service";
import { QueueTypes } from "shared/enums";

const router: Router = Router();

router.get("/", utils.apiErrorCatchAsync(async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    if (req.headers["x-channel-id"])
        throw new Error(`x-channel-id is not specified`);

    let query = req.query;
    let job: any = null

    let channel = req.headers["x-channel-id"] as string;
    const QueueServiceInstance = QueueService.bootstrap();
    QueueServiceInstance.instantiateQueues(channel);

    if (query.queue === QueueTypes.MAIL) {
        let mailQueue = await QueueServiceInstance.getQueue(query.queue as QueueTypes)
        if (mailQueue)
            job = await mailQueue.getJob(query.jobId as string)
        else
            throw new Error(`Unknown job: ${query.queue}`)
    }
    else
        throw new Error("Invalid queue")

    res.status(200).json({
        status: 200,
        message: "ok",
        data: { job }
    })
}))


export default router