# Setup the project

## Step 1: Generating oauthMailToken.json
1. Make sure to install node and open the terminal
2. Run ```npm install ```
3. Run ```mkdir .gcp``` and place the downloaded GCP Oauth Json file inside .gcp folder. Rename it as "oauthMailKey.json"
4. Then run ```npm run genOauthToken``` to generate "oauthMailToken.json" in .gcp folder.

## Step 2: Install Redis commander
1. If havent install redis-commander, run ```npm install -g redis-commander```
2. Run ```redis-commander```
3. Open http://127.0.0.1:8081, then we could see our redis data and queue. 

## Step 3: Run the application
1. Run ```npm run api``` for the api (queue producer)
2. Run ```npm run worker app1 app2``` for the worker (queue consumer)
3. Check the api sample in examples/testApi.json, run ```npm run api:test``` to execute.
4. Please customize the examples/mailTemplate.ts, modify "sender" params to be the email of the respective GCP authorized user who granted the permission to send the email.

## Step 4: Customize for your own application
1. The code is already versatile to be reused in any application.
2. Please pass in "x-channel-id" in the request headers every time you send the mail request.
3. Then, you would need to run the worker based on the "x-channel-id" as Redis prefix passed in to process the queue
4. Request header x-channel-id E.g: ["reminder-app", "inventory-app", "marketing-app"]
5. Pass args in worker E.g: ```npm run worker reminder-app inventory-app marketing-app ...```