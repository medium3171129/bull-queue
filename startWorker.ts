import dotenv from "dotenv";
import startMailWorker from "queue-worker/worker/mail.work";
dotenv.config()

const args = process.argv.slice(2);

for (const channel of args) {
    console.log(`Running worker: ${channel}`)
    startMailWorker(channel)
}